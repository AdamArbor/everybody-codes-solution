using everybody_codes_API.Services;
using Microsoft.AspNetCore.Mvc;

namespace everybody_codes_API.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/camera")]
    public class SearchController : ControllerBase
    {
        [HttpGet("")]
        public async Task<IActionResult> GetAllAsync(
            [FromServices] CameraService service)
        {
            return Ok(service.GetAllCamerasAsync());
        }

        [HttpGet("search")]
        public async Task<IActionResult> SearchAsync(
            [FromServices] CameraService service,
            [FromQuery] string name)
        {
            return Ok(service.GetCamerasByNameAsync(name));
        }
    }
}