﻿using System.Net;

namespace everybody_codes_API.Exceptions
{
    public class CustomException : Exception
    {
        public CustomException(string message, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
            : base(message)
        {
            StatusCode = statusCode;
        }

        public List<string>? ErrorMessages { get; }
        public HttpStatusCode StatusCode { get; }
    }
}