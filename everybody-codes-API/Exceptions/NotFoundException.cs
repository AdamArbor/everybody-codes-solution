﻿using System.Net;

namespace everybody_codes_API.Exceptions
{
    public class NotFoundException : CustomException
    {
        public NotFoundException(string message, HttpStatusCode statusCode = HttpStatusCode.InternalServerError) : base(
            message, statusCode)
        {
        }
    }
}