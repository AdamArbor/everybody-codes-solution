﻿using System.Net;
using everybody_codes_API.Exceptions;

namespace everybody_codes_API.Middleware
{
    public class ExceptionHandler : IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception exception)
            {
                if (exception is not CustomException && exception.InnerException != null)
                    while (exception.InnerException != null)
                        exception = exception.InnerException;

                var response = context.Response;
                response.ContentType = "application/json";

                response.StatusCode = exception switch
                {
                    NotFoundException => (int)HttpStatusCode.NotFound,
                    CustomException => (int)HttpStatusCode.InternalServerError,
                    _ => (int)HttpStatusCode.InternalServerError
                };
            }
        }
    }
}