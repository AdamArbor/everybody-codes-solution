﻿using everybody_codes_API.Models.DAO;
using everybody_codes_API.Models.DTO;
using everybody_codes_API.Services.Mappers.Interfaces;

namespace everybody_codes_API.Services.Mappers
{
    public class CamerasMapper : IMapper<CamerasDTO, IEnumerable<CameraDAO>>
    {
        public CamerasDTO toDTO(IEnumerable<CameraDAO> DAOs)
        {
            var cameras = DAOs.Select(camera => new CameraDTO
                {
                    Id = camera.Id, Name = camera.CameraName, Latitude = camera.Latitude, Longitude = camera.Longitude
                })
                .ToList();

            return new CamerasDTO
            {
                Cameras = cameras
            };
        }
    }
}