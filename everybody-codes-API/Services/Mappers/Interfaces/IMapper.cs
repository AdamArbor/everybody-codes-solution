﻿namespace everybody_codes_API.Services.Mappers.Interfaces
{
    public interface IMapper<out TReturn, in TParam>
        where TReturn : class
    {
        public TReturn toDTO(TParam param);
    }
}