﻿using System.Net;
using everybody_codes_API.DataAccess.Stores.Interfaces;
using everybody_codes_API.Exceptions;
using everybody_codes_API.Models.DAO;
using everybody_codes_API.Models.DTO;
using everybody_codes_API.Services.Mappers.Interfaces;

namespace everybody_codes_API.Services
{
    public class CameraService
    {
        private readonly IDataStore _dataStore;
        private readonly IMapper<CamerasDTO, IEnumerable<CameraDAO>> _mapper;

        public CameraService(IDataStore dataStore, IMapper<CamerasDTO, IEnumerable<CameraDAO>> mapper)
        {
            _dataStore = dataStore;
            _mapper = mapper;
        }

        public CamerasDTO GetAllCamerasAsync()
        {
            var result = _dataStore.GetAllCameras();
            if (result.Count == 0)
                throw new Exception();
            return _mapper.toDTO(result);
        }

        public CamerasDTO GetCamerasByNameAsync(string name)
        {
            var result = _dataStore.GetCameras(name);
            if (result.Count == 0)
                throw new NotFoundException("", HttpStatusCode.NotFound);
            return _mapper.toDTO(result);
        }
    }
}