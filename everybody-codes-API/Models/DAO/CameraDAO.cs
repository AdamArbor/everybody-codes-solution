﻿using CsvHelper.Configuration.Attributes;

namespace everybody_codes_API.Models.DAO
{
    public class CameraDAO
    {
        [Ignore] public string? Id { get; set; }

        public string? CameraName { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
    }
}