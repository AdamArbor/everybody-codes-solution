﻿namespace everybody_codes_API.Models.DTO
{
    public class CamerasDTO
    {
        public List<CameraDTO>? Cameras { get; set; }
    }
}