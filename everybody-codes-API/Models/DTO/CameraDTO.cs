﻿namespace everybody_codes_API.Models.DTO
{
    public class CameraDTO
    {
        public string? Id { get; set; }
        public string? Name { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
    }
}