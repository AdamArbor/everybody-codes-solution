﻿using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;
using everybody_codes_API.DataAccess.Mappings;
using everybody_codes_API.DataAccess.Stores.Interfaces;
using everybody_codes_API.Models.DAO;

namespace everybody_codes_API.DataAccess.Stores
{
    public class CsvStore : IDataStore
    {
        private readonly CsvReader _csvReader;

        public CsvStore()
        {
            var reader = new StreamReader(".\\cameras-defb.csv");
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = ";",
                MissingFieldFound = null,
                HasHeaderRecord = true,
                IgnoreBlankLines = true
            };
            _csvReader = new CsvReader(reader, config);
        }

        public List<CameraDAO> GetCameras(string searchQuery)
        {
            using (_csvReader)
            {
                _csvReader.Context.RegisterClassMap<CameraCsvMap>();
                var records = _csvReader.GetRecords<CameraDAO>();
                var result = new List<CameraDAO>();
                foreach (var r in records)
                {
                    if (r.CameraName == null || !r.CameraName.Contains(searchQuery)) continue;
                    r.Id = r.CameraName.Substring(7, 3);
                    result.Add(r);
                }

                return result;
            }
        }

        public List<CameraDAO> GetAllCameras()
        {
            using (_csvReader)
            {
                _csvReader.Context.RegisterClassMap<CameraCsvMap>();
                var records = _csvReader.GetRecords<CameraDAO>().ToList();
                foreach (var record in records) record.Id = record.CameraName?.Substring(7, 3);
                return records.ToList();
            }
        }
    }
}