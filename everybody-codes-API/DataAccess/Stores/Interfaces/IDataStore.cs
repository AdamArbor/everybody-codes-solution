﻿using everybody_codes_API.Models.DAO;

namespace everybody_codes_API.DataAccess.Stores.Interfaces
{
    public interface IDataStore
    {
        public List<CameraDAO> GetCameras(string searchQuery);
        public List<CameraDAO> GetAllCameras();
    }
}