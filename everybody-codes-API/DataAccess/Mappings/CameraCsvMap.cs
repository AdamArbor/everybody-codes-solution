﻿using CsvHelper.Configuration;
using everybody_codes_API.Models.DAO;

namespace everybody_codes_API.DataAccess.Mappings
{
    public sealed class CameraCsvMap : ClassMap<CameraDAO>
    {
        public CameraCsvMap()
        {
            Map(m => m.CameraName).Name("Camera");
            Map(m => m.Latitude).Name("Latitude");
            Map(m => m.Longitude).Name("Longitude");
        }
    }
}