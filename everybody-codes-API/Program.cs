using everybody_codes_API.DataAccess.Stores;
using everybody_codes_API.DataAccess.Stores.Interfaces;
using everybody_codes_API.Middleware;
using everybody_codes_API.Models.DAO;
using everybody_codes_API.Models.DTO;
using everybody_codes_API.Services;
using everybody_codes_API.Services.Mappers;
using everybody_codes_API.Services.Mappers.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddTransient<IMapper<CamerasDTO, IEnumerable<CameraDAO>>, CamerasMapper>();
builder.Services.AddTransient<IDataStore, CsvStore>();
builder.Services.AddTransient<CameraService>();
builder.Services.AddTransient<ExceptionHandler>();

var app = builder.Build();

app.UseMiddleware<ExceptionHandler>();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();