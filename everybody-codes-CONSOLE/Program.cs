﻿using everybody_codes_CONSOLE.Data;
using everybody_codes_CONSOLE.Data.Interfaces;

namespace everybody_codes_CONSOLE
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("Error Message.");
                throw new NotSupportedException();
            }

            var command = args[0];
            var flag = args[1];
            var argument = args[2];
            CommandRunner(command, flag, argument);
        }

        private static void CommandRunner(string command, string flag, string argument)
        {
            switch (command)
            {
                case "Search":
                    SearchCommand(flag, argument);
                    break;
            }
        }

        private static void SearchCommand(string flag, string argument)
        {
            IDataStore dataStore = new CsvStore();
            if (flag.Equals("--name"))
            {
                var cameras = dataStore.GetCamera(argument);
                foreach (var camera in cameras)
                    Console.WriteLine($"{camera.Id} | {camera.CameraName} | {camera.Latitude} | {camera.Longitude}");
            }
        }
    }
}