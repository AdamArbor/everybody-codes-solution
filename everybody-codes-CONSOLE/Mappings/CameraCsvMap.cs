﻿using CsvHelper.Configuration;
using everybody_codes_CONSOLE.Models;

namespace everybody_codes_CONSOLE.Mappings
{
    public sealed class CameraCsvMap : ClassMap<Camera>
    {
        public CameraCsvMap()
        {
            Map(m => m.CameraName).Name("Camera");
            Map(m => m.Latitude).Name("Latitude");
            Map(m => m.Longitude).Name("Longitude");
        }
    }
}