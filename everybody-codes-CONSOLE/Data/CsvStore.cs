﻿using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;
using everybody_codes_CONSOLE.Data.Interfaces;
using everybody_codes_CONSOLE.Mappings;
using everybody_codes_CONSOLE.Models;

namespace everybody_codes_CONSOLE.Data
{
    public class CsvStore : IDataStore
    {
        private readonly CsvReader _csvReader;

        public CsvStore()
        {
            var reader = new StreamReader(".\\cameras-defb.csv");
            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = ";",
                MissingFieldFound = null,
                HasHeaderRecord = true,
                IgnoreBlankLines = true
            };
            _csvReader = new CsvReader(reader, config);
        }

        public List<Camera> GetCamera(string searchQuery)
        {
            using (_csvReader)
            {
                _csvReader.Context.RegisterClassMap<CameraCsvMap>();
                var records = _csvReader.GetRecords<Camera>();
                var result = new List<Camera>();
                foreach (var r in records)
                {
                    if (r.CameraName == null || !r.CameraName.Contains(searchQuery)) continue;
                    r.Id = r.CameraName.Substring(7, 3);
                    result.Add(r);
                }

                return result;
            }
        }
    }
}