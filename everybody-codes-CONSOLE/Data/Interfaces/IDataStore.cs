﻿using everybody_codes_CONSOLE.Models;

namespace everybody_codes_CONSOLE.Data.Interfaces
{
    public interface IDataStore
    {
        public List<Camera> GetCamera(string searchQuery);
    }
}