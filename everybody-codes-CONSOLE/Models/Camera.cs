﻿using CsvHelper.Configuration.Attributes;

namespace everybody_codes_CONSOLE.Models
{
    public class Camera
    {
        [Ignore] public string? Id { get; set; }

        public string? CameraName { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
    }
}